'use strict'
/*
TODO :
    - control input : only two digits after point ('beforeinput' ?) (type="text" ?)
*/
document.addEventListener('DOMContentLoaded', main)

let templateRow
let plus

function main() {
    document.removeEventListener('DOMContentLoaded', main)
    templateRow = document.getElementsByClassName('row')[0].cloneNode(true)
    plus = document.getElementsByClassName('plus')[0]
    addListeners()
    plus.addEventListener('click', plusButtonClicked)
}

function addListeners(parent = document) {
    for (let input of parent.getElementsByTagName('input')) {
        input.addEventListener('change', inputModified)
    }
    parent
        .getElementsByClassName('minus')[0]
        .addEventListener('click', minusButtonClicked)
}

function inputModified(event) {
    let rows = Array.from(document.getElementsByClassName('row'))
    let sums = rows.reduce(function (obj, row) {
        obj.total = (obj.total || 0) + Number(row.children[2].value)
        obj[row.children[1].value] =
            (obj[row.children[1].value] || 0) + Number(row.children[2].value)
        return obj
    }, Object())

    if (sums.total > subtotal.value) {
        subtotal.value = sums.total
    }

    let localPart
    let globalPart
    for (let row of rows) {
        if (row.children[2].value) {
            localPart = row.children[2].value / subtotal.value
            globalPart = sums[row.children[1].value] / subtotal.value

            row.children[3].textContent =
                Math.round(localPart * 100) +
                (row.children[2].value == sums[row.children[1].value]
                    ? ''
                    : ` | ${Math.round(globalPart * 100)}`)

            row.children[4].textContent =
                (total.value
                    ? Math.round(localPart * total.value * 100) / 100
                    : row.children[2].value) +
                (row.children[2].value == sums[row.children[1].value]
                    ? ''
                    : ` | ${
                          total.value
                              ? Math.round(globalPart * total.value * 100) / 100
                              : sums[row.children[1].value]
                      }`)
        } else {
            row.children[3].textContent = row.children[4].textContent =
                templateRow.children[3].textContent
        }
    }

    if (
        event.target.value &&
        event.target.parentElement.nextElementSibling &&
        event.target.parentElement.nextElementSibling.className != 'row'
    ) {
        plus.dispatchEvent(new Event('click'))
    }
}

function plusButtonClicked(event) {
    event.target.parentElement.parentElement.insertBefore(
        templateRow.cloneNode(true),
        event.target.parentElement
    )
    addListeners(event.target.parentElement.previousElementSibling)
}

function minusButtonClicked(event) {
    event.target.parentElement.remove()
    total.dispatchEvent(new Event('change'))
}
